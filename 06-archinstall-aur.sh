#!/bin/bash

sudo mkdir -pv ~/Desktop/AUR
sudo chmod 777 ~/Desktop/AUR
cd ~/Desktop/AUR
sudo git clone https://aur.archlinux.org/package-query 
sudo git clone https://aur.archlinux.org/yay 
sudo git clone https://aur.archlinux.org/yaourt

sudo chmod 777 package-query yay yaourt

cd package-query
makepkg -si

cd ../yay
makepkg -si

cd ../yaourt
makepkg -si

cd ../../
sudo rm -rf /AUR
