#!/bin/bash

sudo git clone https://gitlab.com/shastenm76/dotfiles

chmod 777 dotfiles
cd dotfiles

chmod 777 bashrc
cp -r -i -v bashrc /home/shasten/.bashrc

chmod 777 xinitrc /etc/X11/xinit/xinitrc
cp -r -i -v xinitrc /etc/X11/xinit/xinitrc

chmod 777 aliasrc
cp -r -i -v aliasrc ~/.config/aliasrc

chmod 777 shortcutrc
cp -r -i -v shortcutrc ~/.config/shortcutrc

chmod 777 bash_profile
cp -r -i -v bash_profile ~/.bash_profile

chmod 777 Xresources
cp -r -i -v Xresources ~/.Xresources

cd ../
sudo rm -r dotfiles

