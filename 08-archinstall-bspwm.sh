#!/bin/bash

sudo git clone https://gitlab.com/shastenm76/bspwm

sudo chmod 777 bspwm
cd bspwm
sudo chmod 777 bspwmrc sxhkdrc fehbg

sudo mv /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm/
sudo mv /usr/share/doc/bspwm/examples/sxhkdrc ~/.config/sxhkdrc

