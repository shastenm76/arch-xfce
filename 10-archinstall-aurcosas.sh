#!/bin/bash

yay -S brave-bin spotify blueman mpd ncmpcpp hexchat compiz emerald exfat-utils

yay xf86-input-synaptics
yay pulseaudio
yay gvfs
yay gst-plugins
yay obs-studio
