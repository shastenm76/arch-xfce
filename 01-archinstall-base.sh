#!/bin/bash

#Arch Installation Part 1

# wifi-menu

# configure partition configuration

pacstrap /mnt base base-devel grub os-prober bash-completion dialog networkmanager linux-lts linux-lts-headers xf86-input-synaptics intel-ucode xorg

genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt

sleep 5s

cd /home/Desktop/arch-xfce
./02-archinstall-grub.sh